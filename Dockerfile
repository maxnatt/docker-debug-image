FROM rockylinux:9

# hadolint ignore=DL3041
RUN dnf module enable -y postgresql:15 && \
  dnf install --nodocs -y \
  bind-utils \
  mtr \
  mysql \
  nmap-ncat \
  openssh-clients \
  openssl \
  postgresql \
  procps-ng \
  redis \
  telnet \
  tmux \
  vim && \
  dnf clean all
